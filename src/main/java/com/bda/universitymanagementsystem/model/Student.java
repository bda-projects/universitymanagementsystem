package com.bda.universitymanagementsystem.model;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)

public class Student {
    Long id;
    String name;
    String surname;
    String username;
    String password;
    String subject;
    Long groupId;
    Long roleId;

}
