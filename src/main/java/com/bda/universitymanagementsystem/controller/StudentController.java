package com.bda.universitymanagementsystem.controller;

import com.bda.universitymanagementsystem.entity.StudentEntity;
import com.bda.universitymanagementsystem.service.StudentServices;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class StudentController {
    @Autowired
    StudentServices studentServices;

    @GetMapping("/students")
    public List<StudentEntity> getStudents(){
        return studentServices.getStudents();
    }

    @PostMapping("/admin/create-student")
    public void createStudent(){
        studentServices.createStudent();
    }
}
