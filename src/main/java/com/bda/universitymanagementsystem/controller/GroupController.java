package com.bda.universitymanagementsystem.controller;


import com.bda.universitymanagementsystem.entity.GroupEntity;
import com.bda.universitymanagementsystem.service.GroupServices;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
//@RequestMapping("")
public class GroupController {

    @Autowired
    GroupServices groupServices;

    @GetMapping("/groups")
    public List<GroupEntity> getGroups(){
        return groupServices.getGroups();
    }


    @PostMapping("/admin/create-group")
    public void createGroup(){
        groupServices.createGroup();
    }
}
