package com.bda.universitymanagementsystem.service;

import com.bda.universitymanagementsystem.entity.JournalEntity;

import java.util.List;

public interface JournalServices {
    void createJournal();
    List<JournalEntity> getJournals();
}
