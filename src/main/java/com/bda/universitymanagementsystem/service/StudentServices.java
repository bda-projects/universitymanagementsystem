package com.bda.universitymanagementsystem.service;

import com.bda.universitymanagementsystem.entity.StudentEntity;

import java.util.List;

public interface StudentServices {

    void createStudent();
    List<StudentEntity> getStudents();
}
