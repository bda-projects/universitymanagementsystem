package com.bda.universitymanagementsystem.service;

public interface UserServices {
    void createUser(String name, String surname, String username, String password, Long roleId);
}
