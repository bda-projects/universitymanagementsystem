package com.bda.universitymanagementsystem.service;

import com.bda.universitymanagementsystem.entity.AdminEntity;

import java.util.List;

public interface AdminServices {


    void createAdmin();
    List<AdminEntity> getAdmins();


}
