package com.bda.universitymanagementsystem.service;

import com.bda.universitymanagementsystem.entity.GroupEntity;

import java.util.List;

public interface GroupServices {
    void createGroup();
    List<GroupEntity> getGroups();
}
