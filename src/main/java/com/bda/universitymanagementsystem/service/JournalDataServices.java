package com.bda.universitymanagementsystem.service;

import com.bda.universitymanagementsystem.entity.JournalDataEntity;

import java.util.List;

public interface JournalDataServices {
    void createJournalData();
    List<JournalDataEntity> getJournalDatas();
}
