package com.bda.universitymanagementsystem.service.impl;


import com.bda.universitymanagementsystem.entity.GroupDataEntity;
import com.bda.universitymanagementsystem.entity.GroupEntity;
import com.bda.universitymanagementsystem.entity.TeacherEntity;
import com.bda.universitymanagementsystem.repository.GroupDataRepository;
import com.bda.universitymanagementsystem.repository.GroupRepository;
import com.bda.universitymanagementsystem.repository.TeacherRepository;
import com.bda.universitymanagementsystem.service.GroupDataServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

@Service
public class GroupDataServicesImpl implements GroupDataServices {
    static Scanner scanner = new Scanner(System.in);

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    TeacherRepository teacherRepository;

    @Autowired
    GroupDataRepository groupDataRepository;

    public void createGroupData(){
        System.out.println("-------------------------");
        System.out.println("Qrupa muellimin elave edilmesi");
        System.out.println("-------------------------");
        System.out.println("Qrupun id-sini yazin:");
        Long groupId = scanner.nextLong();
        System.out.println("Muellimin id-sini yazin:");
        Long teacherId = scanner.nextLong();

        GroupEntity group = new GroupEntity();
        Optional<GroupEntity> optionalGroup = groupRepository.findById(groupId);
        String groupName = optionalGroup.stream().map(item-> item.getName() ).toString();
        group.setId(groupId);
        group.setName(groupName);

        TeacherEntity teacher = new TeacherEntity();
        Optional<TeacherEntity> optionalTeacher = teacherRepository.findById(teacherId);
        String name = optionalTeacher.stream().map(item-> item.getName()).toString();
        String surname = optionalTeacher.stream().map(item-> item.getSurname()).toString();
        String username = optionalTeacher.stream().map(item-> item.getUsername()).toString();
        String subject = optionalTeacher.stream().map(item-> item.getSubject()).toString();
        teacher.setName(name);
        teacher.setSurname(surname);
        teacher.setUsername(username);
        teacher.setPassword(null);
        teacher.setSubject(subject);
        teacher.setRoleId(2L);


        GroupDataEntity groupData = new GroupDataEntity(null,group,teacher);
    }

    public List<GroupDataEntity> getGroupDatas(){
        return groupDataRepository.findAll();
    }
}
