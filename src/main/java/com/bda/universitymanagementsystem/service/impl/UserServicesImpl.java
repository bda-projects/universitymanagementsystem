package com.bda.universitymanagementsystem.service.impl;

import com.bda.universitymanagementsystem.entity.UserEntity;
import com.bda.universitymanagementsystem.repository.UserRepository;
import com.bda.universitymanagementsystem.service.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServicesImpl implements UserServices {
    @Autowired
    UserRepository userRepository;


    public void createUser(String name, String surname, String username, String password, Long roleId){
        UserEntity user = new UserEntity(null,name,surname,username,password,roleId);
        userRepository.save(user);
    }
}
