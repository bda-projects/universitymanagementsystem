package com.bda.universitymanagementsystem.service.impl;

import com.bda.universitymanagementsystem.entity.GroupEntity;
import com.bda.universitymanagementsystem.entity.JournalEntity;
import com.bda.universitymanagementsystem.repository.GroupRepository;
import com.bda.universitymanagementsystem.repository.JournalRepository;
import com.bda.universitymanagementsystem.service.JournalServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

@Service
public class JournalServicesImpl implements JournalServices {
    static Scanner scanner = new Scanner(System.in);

    @Autowired
    JournalRepository journalRepository;

    @Autowired
    GroupRepository groupRepository;

    public void createJournal(){
        System.out.println("-------------------------");
        System.out.println("Jurnal elave edilmesi");
        System.out.println("-------------------------");
        System.out.println("Hansi qrupun jurnali olacaq?");
        Long groupId = scanner.nextLong();

        GroupEntity group = new GroupEntity();
        group.setId(groupId);

        Optional<GroupEntity> optionalGroup = groupRepository.findById(groupId);
        String groupName = optionalGroup.stream().map(item-> item.getName()).toString();
        group.setName(groupName);

        JournalEntity journal = new JournalEntity(null, group);
        journalRepository.save(journal);
    }

    public List<JournalEntity> getJournals(){
        return journalRepository.findAll();
    }
}
