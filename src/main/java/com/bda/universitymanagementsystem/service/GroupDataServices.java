package com.bda.universitymanagementsystem.service;

import com.bda.universitymanagementsystem.entity.GroupDataEntity;

import java.util.List;

public interface GroupDataServices {
    void createGroupData();
    List<GroupDataEntity> getGroupDatas();
}
