package com.bda.universitymanagementsystem.service;

import com.bda.universitymanagementsystem.entity.TeacherEntity;

import java.util.List;

public interface TeacherServices {
    void createTeacher();
    List<TeacherEntity> getTeachers();
}
