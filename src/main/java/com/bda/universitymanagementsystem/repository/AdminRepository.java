package com.bda.universitymanagementsystem.repository;



import com.bda.universitymanagementsystem.entity.AdminEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminRepository extends JpaRepository<AdminEntity, Long> {
}
