package com.bda.universitymanagementsystem.repository;

import com.bda.universitymanagementsystem.entity.JournalEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JournalRepository extends JpaRepository<JournalEntity, Long> {
}
