package com.bda.universitymanagementsystem.repository;

import com.bda.universitymanagementsystem.entity.GroupDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupDataRepository extends JpaRepository<GroupDataEntity, Long> {
}
