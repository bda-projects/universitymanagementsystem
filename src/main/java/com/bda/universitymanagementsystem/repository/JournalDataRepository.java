package com.bda.universitymanagementsystem.repository;

import com.bda.universitymanagementsystem.entity.JournalDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JournalDataRepository extends JpaRepository<JournalDataEntity, Long> {
}
