package com.bda.universitymanagementsystem.repository;

import com.bda.universitymanagementsystem.entity.GroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupRepository extends JpaRepository<GroupEntity, Long> {
}
