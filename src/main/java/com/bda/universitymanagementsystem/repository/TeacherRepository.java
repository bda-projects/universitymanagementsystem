package com.bda.universitymanagementsystem.repository;

import com.bda.universitymanagementsystem.entity.TeacherEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherRepository extends JpaRepository<TeacherEntity, Long> {
}
