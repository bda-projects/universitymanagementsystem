package com.bda.universitymanagementsystem.repository;

import com.bda.universitymanagementsystem.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
}
